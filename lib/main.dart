import 'package:flutter/material.dart';

import './quiz.dart';
import './result.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _MyAppState();
  }
}

class _MyAppState extends State<MyApp> {
  var _questionIndex = 0;
  var _totalScore = 0;

  _answerQuestion(int score) {
    setState(() {
      _questionIndex++;
      _totalScore += score;
    });
    print(_questionIndex);
  }

  _restartQuiz() {
    setState(() {
      _questionIndex = 0;
      _totalScore = 0;
    });
  }

  @override
  Widget build(BuildContext context) {
    var _questions = [
      {
        'questionText': 'สีที่คุณชอบ ?',
        'answers': [
          {'text': 'น้ำเงิน', 'score': 2, 'buttonColor': Colors.blue},
          {'text': 'เขียว', 'score': 3, 'buttonColor': Colors.green},
          {'text': 'ม่วง', 'score': 6, 'buttonColor': Colors.purple},
          {
            'text': 'เหลือง',
            'score': 2,
            'buttonColor': Colors.yellow,
            'textColor': Colors.black
          },
          {'text': 'แดง', 'score': 7, 'buttonColor': Colors.red},
          {
            'text': 'ขาว',
            'score': 0,
            'buttonColor': Colors.white,
            'textColor': Colors.black
          },
          {'text': 'ดำ', 'score': 10, 'buttonColor': Colors.black},
        ]
      },
      {
        'questionText': 'สัตว์ที่คุณชอบ ?',
        'answers': [
          {'text': 'แมงมุม', 'score': 10, 'buttonColor': Colors.black},
          {
            'text': 'กระต่าย',
            'score': 0,
            'buttonColor': Colors.grey,
          },
          {'text': 'งู', 'score': 10, 'buttonColor': Colors.red},
          {
            'text': 'หงษ์',
            'score': 0,
            'buttonColor': Colors.white,
            'textColor': Colors.black
          },
          {
            'text': 'สุนัข',
            'score': 4,
            'buttonColor': Colors.yellow,
            'textColor': Colors.black
          },
        ]
      },
      {
        'questionText': 'เครื่องดื่มที่คุณชอบ ?',
        'answers': [
          {
            'text': 'เบียร์',
            'score': 8,
            'buttonColor': Colors.yellow,
            'textColor': Colors.black
          },
          {'text': 'ไวน์', 'score': 7, 'buttonColor': Colors.red},
          {
            'text': 'ชา',
            'score': 3,
            'buttonColor': Colors.grey,
          },
          {
            'text': 'นม',
            'score': 2,
            'buttonColor': Colors.white,
            'textColor': Colors.black
          },
          {'text': 'กาแฟ', 'score': 5, 'buttonColor': Colors.black},
          {
            'text': 'น้ำเปล่า',
            'score': 0,
            'buttonColor': Colors.white,
            'textColor': Colors.black
          },
          {'text': 'วอดก้า', 'score': 10, 'buttonColor': Colors.blue},
        ]
      }
    ];

    return MaterialApp(
      theme: ThemeData(primaryColor: Colors.green),
      home: Scaffold(
          backgroundColor: Colors.orange,
          appBar: AppBar(
            title: Column(
              children: <Widget>[
                Text(
                  'Personality Quiz',
                  style: TextStyle(fontSize: 24),
                ),
                Text(
                  'ค้นหาตัวตนของคุณจาก 3 สิ่ง',
                  style: TextStyle(fontSize: 14),
                )
              ],
            ),
          ),
          body: _questionIndex < _questions.length
              ? Quiz(
                  answerQuestion: _answerQuestion,
                  questionIndex: _questionIndex,
                  questions: _questions,
                )
              : Result(_totalScore, _restartQuiz)),
    );
  }
}
