import 'package:flutter/material.dart';

class Result extends StatelessWidget {
  final int resultScore;
  final Function restartQuiz;
  Result(this.resultScore, this.restartQuiz);

  Map<String, Object> get result {
    String text;
    Color textColor;
    Color background;
    if (resultScore <= 8) {
      text = 'คุณเป็นคนอ่อนโยน, บริสุทธิ์, ใสซื่อและเงียบงัน';
      background = Colors.white;
      textColor = Colors.pink;
    } else if (resultScore <= 12) {
      text = 'คุณค่อนข้างน่ารัก, รักสนุกและมีชีวิตชีวา!';
      background = Colors.orange;
      textColor = Colors.green;
    } else if (resultScore <= 16) {
      text = 'บางทีคุณก็ประหม่า, บางทีก็ทำตัวแปลกๆ';
      background = Colors.red;
      textColor = Colors.white;
    } else {
      text = 'คุณเป็นคนรุนแรง, โกรธบ่อย, หรือบางทีก็เป็นคนอันตราย';
      background = Colors.black;
      textColor = Colors.red;
    }
    return {'text': text, 'textColor': textColor, 'background': background};
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        // alignment: Alignment.center,

        color: result['background'],
        width: double.infinity,
        padding: EdgeInsets.all(50),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          // crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Text(
              result['text'],
              style: TextStyle(
                  fontSize: 36,
                  color: result['textColor'],
                  fontWeight: FontWeight.bold),
              textAlign: TextAlign.center,
            ),
            Container(
              margin: EdgeInsets.only(top: 50),
              // margin: EdgeInsets.all(20),
              child: RaisedButton(
                child: Text('Start again'),
                color: Colors.green,
                textColor: Colors.white,
                onPressed: restartQuiz,
              ),
            )
          ],
        ),
      ),
    );
  }
}
